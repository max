{-----
 This file has been released by its author to the Public Domain.
 Details in doc/cc0.txt relative to the top level of this distribution,
 or at https://creativecommons.org/publicdomain/zero/1.0/
 -----}
module Main where

import Max
import Max.Nexus
import Max.Interpreter
import Control.Concurrent.STM
import System.ZMQ4.Monadic
import Data.Acid
import Data.Acid.Advanced (query', update')
import Data.SafeCopy
import Control.Concurrent (threadDelay, getNumCapabilities, setNumCapabilities)
import Data.Default (def)
import Control.Monad (forever, forM_, when)
import System.Posix.Daemon (runDetached, Redirection(DevNull))
import Control.Exception (finally)
import System.Directory (doesFileExist, removeFile)
import System.Posix.Signals (installHandler, sigTERM, Handler(Catch))
import System.Posix.Process.ByteString (exitImmediately)
import System.Exit (ExitCode(ExitSuccess))
import Data.Serialize (encode, decode)
import Data.ByteString.Char8 (pack, unpack)
import Data.List (genericIndex)

pidfile = "maxd.pid"
worldDataPath = "world"

main :: IO ()
main = do
  nCapabilities <- getNumCapabilities
  setNumCapabilities 1 -- Prevent stuck parent process from pthread deadlock during runDetached/forkProcess [1]
  oldhandler <- installHandler sigTERM (Catch cleanup) Nothing
  runDetached (Just pidfile) def $
    flip finally
      (ensureRemoved pidfile)
      (setNumCapabilities nCapabilities >> daemon_main)
  exitImmediately ExitSuccess

ensureRemoved :: FilePath -> IO ()
ensureRemoved path = do
    fileExists <- doesFileExist path
    when fileExists (removeFile path)

cleanup = do
  ensureRemoved pidfile
  exitImmediately ExitSuccess

-- Start both switches On
initialWorld = World [ SceneObject { obj=Lamp True,   pos=Position { x=0.1, y=0.0, z=0.0 }}
                     , SceneObject { obj=Switch True, pos=Position { x=(15.0), y=0.0, z=0.0 }}
                     , SceneObject { obj=Switch True, pos=Position { x=(30.0), y=0.0, z=0.0 }}
                     , SceneObject { obj=(Text "foo"),  pos=Position { x=45.0, y=5.0, z=0.0 }}
                     ]

-- index of the lamp in the above list
nandOutputWorldIndex = 0 :: Integer
-- index of the switches in the above list
nandInputAworldIndex = 1 :: Integer
nandInputBworldIndex = 2 :: Integer

orGate = MachineGraph (
  [InterfaceNode ([MaxBool, MaxBool], [MaxBool]),
   PrimitiveNode NandGate,
   PrimitiveNode NandGate,
   PrimitiveNode NandGate],
  [(0,0,2,0),(0,0,2,1),
   (0,1,3,0),(0,1,3,1),
   (2,0,1,0),(3,0,1,1),
   (1,0,0,0)])

initializeSockets :: ZMQ z (Socket z Router, Socket z Pub)
initializeSockets = do
    {- Setup a socket to receive commands from clients. Use Router instead of Req
       to allow multiple commands from the same client in parallel.
     -}
    zmqRouterSock <- socket Router
    setIpv6 True zmqRouterSock
    setRouterMandatory True zmqRouterSock -- enable checking for unroutable peer identity. TODO: handle EHOSTUNREACH error when writing to Router socket
    bind zmqRouterSock "tcp://*:3333"

    {- Setup a socket to send announce-only world updates to possibly many clients,
       using potentially high bandwidth, so use Pub. Potential for multicast later.
     -}
    zmqPubSock <- socket Pub
    setIpv6 True zmqPubSock
    bind zmqPubSock "tcp://*:3334"

    return (zmqRouterSock, zmqPubSock)

daemon_main = do
  worldState <- openLocalStateFrom worldDataPath initialWorld

  let impl = construct orGate
  machine <- simulate impl
  putStrLn("simulation launched")

  runZMQ $ do
    (zmqRouterSock, zmqPubSock) <- initializeSockets
    -- run one world update to ensure the lamp is set correctly from the switches.
    -- Incorrect state could be set by the initialWorld or an incorrect DB state.
    updateWorld zmqRouterSock zmqPubSock worldState machine
    forever (handleCommand zmqRouterSock zmqPubSock worldState machine)

--sendError :: String -> Socket z Router -> ByteString -> ZMQ z ()
sendError s zmqRouterSock clientId = do
  liftIO $ putStrLn ("ServError: " ++ s)
  send zmqRouterSock [SendMore] clientId
  send zmqRouterSock [] (encode False)
  return ()

handleCommand :: Socket z Router -> Socket z Pub ->
                 AcidState World ->
                 ([Bool -> STM ()], [STM (Maybe Bool)]) ->
                 ZMQ z ()
handleCommand zmqRouterSock zmqPubSock worldState machine = do
  clientId <- receive zmqRouterSock -- first frame: reply envelope
  buffer <- receive zmqRouterSock

  let decodedbuffer = decode buffer
  case decodedbuffer of

    Left s -> sendError s zmqRouterSock clientId

    Right cmd -> do
      case (unpack cmd) of
        "getWorldState" -> do
          liftIO $ putStrLn "received getWorldState"
          send zmqRouterSock [SendMore] clientId
          world <- query' worldState WorldList
          send zmqRouterSock [] (encode world)
          return ()

        "tryToggle" -> do
          liftIO $ putStrLn "received toggle"
          handleToggle zmqRouterSock zmqPubSock clientId worldState
          updateWorld zmqRouterSock zmqPubSock worldState machine
          send zmqRouterSock [SendMore] clientId
          send zmqRouterSock [] (encode True)
          return ()

        "tryAddObject" -> do
          liftIO $ putStrLn "received tryAddObject"
          bufObj <- receive zmqRouterSock
          let decodedObj = decode bufObj
          case decodedObj of
            Left s -> do
              liftIO $ putStrLn ("Unknown object, error: " ++ show s)
              sendError s zmqRouterSock clientId
              return ()
            Right obj -> do
              -- add the object to the World using an AcidState Update monad
              liftIO $ putStrLn ("Adding object to world: " ++ show obj)
              update' worldState (AddObject obj)
              liftIO $ putStrLn ("tryAddObject: world updated")
              send zmqRouterSock [SendMore] clientId
              send zmqRouterSock [] (encode True)
              liftIO $ putStrLn ("tryAddObject: sent True to client")
              send zmqPubSock [SendMore] (encode $ pack "addedObject")
              {- optimize: We didn't modify the object client sent us. Send it right back out without re-encoding -}
              send zmqPubSock [] bufObj
              return ()

        str -> do
          liftIO $ putStrLn ("unknown message from client: " ++ str)
          -- TODO: to handle unknown message w/o hanging, read the rest of the ZMQ message parts here.
          return ()

{- Read the sceneId to toggle from the zmqRouterSock.
   Toggle it if appropriate. -}
handleToggle zmqRouterSock zmqPubSock clientId worldState = do
  buffer2 <- receive zmqRouterSock
  let decodedbuffer = decode buffer2
  case decodedbuffer of
    Left s -> sendError s zmqRouterSock clientId
    Right switchId -> do
      let foo = switchId :: Integer -- specify type to compiler in case the below putStrLn is removed.
      liftIO $ putStrLn $ "Received message: " ++ show (switchId::Integer)
      update' worldState (ToggleSceneObject switchId)
      send zmqPubSock [SendMore] (encode $ pack "toggled")
      send zmqPubSock [] (encode switchId)

updateWorld zmqRouterSock zmqPubSock worldState machine = do
  {- simulate the world -}
  scene <- query' worldState WorldList
  let (SceneObject {obj=(Switch a)})     = scene `genericIndex` nandInputAworldIndex
      (SceneObject {obj=(Switch b)})     = scene `genericIndex` nandInputBworldIndex
      (SceneObject {obj=(Lamp oldNand)}) = scene `genericIndex` nandOutputWorldIndex

  liftIO $ putStrLn ("Old world state: a=" ++ show a ++ ", b=" ++ show b ++ ", oldNand=" ++ show oldNand)
  c <- queryMachine machine a b
  update' worldState (SetLamp c)
  liftIO $ putStrLn ("New world state: a=" ++ show a ++ ", b=" ++ show b ++ ", oldNand=" ++ show oldNand)

  {- if client actions affected the world, then notify the client -}
  if oldNand /= c then do
    send zmqPubSock [SendMore] (encode $ pack "toggled")
    send zmqPubSock [] (encode nandOutputWorldIndex)
  else
    return ()

queryMachine ([inputA, inputB], [output]) a b = do
  liftIO $ atomically $ do
    inputA a
    inputB b

  liftIO $ putStrLn ("sent inputs")

  mc <- liftIO $ atomically output
  case mc of
    Nothing -> error "Machine pipe has been Closed"
    Just c -> do
      liftIO $ putStrLn $ "Received result: " ++ show c
      return c

{-

Footnote

1. (thanks Geekosaur): with the multithreaded Haskell runtime (as of GHC 7.10.3), 
   runDetached may deadlock, resulting in an extra process sticking around which should have exited.
   Both Hackage libraries, "hdaemonize" and "daemons", use forkProcess and are subject to this bug.
   ForkProcess uses a 2-phase fork, and it's the first process that deadlocks. The second process
   continues on to launch the daemon, which behaves normally. The deadlock occurs within the pthread library,
   within pthread_mutex_lock, called from NewTask, called from startWorkerTask, called from forkProcess.
   This is easily verifiable by making a minimal daemon with runDetached and launching it repeatedly:

import System.Posix.Daemon (runDetached)
import Control.Concurrent (threadDelay)
import Data.Default (def)
import Control.Monad (forever)

main :: IO ()
main = runDetached (Just "testdaemon.pid") def $ forever $ threadDelay 1000000

   or

import System.Posix.Daemonize
import Control.Monad (forever)

main :: IO ()
main = daemonize (forever $ return ())

   We avoid this by ensuring only one Haskell "thread" (capability) is running during runDetached.
   See discussion between Tril and Geekosaur on the Freenode #haskell IRC channel on 2016.05.07:
   http://tunes.org/~nef/logs/haskell/16.05.07

   Relevant conversation edited from the full log:

20:59:11 <Tril> summary: looks like the 2 haskell daemon libraries I tried either both use forkProcess wrong or there's an issue in forkProcess.
21:00:39 <geekosaur> ghc version?
21:00:57 <Tril> 7.10.3, I think also 7.10.2
21:01:32 <geekosaur> (also I have built and run xmonad using both debian/ubuntu ghc 7.6.3 and hvr's 7.10.3 PPA, it uses double-forking heavily, no locking issues)
21:03:35 <geekosaur> oh. I think I looked at the code of one of those once and saw some iffy things, but I don't recall details. many people do not get the POSIX process model, and it will bite you HARD if you don't.
21:03:50 <geekosaur> ...oh wait, this is locking in pthread. I think that is a known issue
21:04:43 <geekosaur> fork() itself is dubious with multiple threads; forkProcess is even more so and I think they are aware that it's buggy with the threaded runtime. (xmonad doesn't use the threaded runtime because X11 isn't really thread safe)
21:11:52 <Tril> geekosaur: so if I want to make a daemon in Haskell and shouldn't use forkProcess which library should I call?
21:12:10 <geekosaur> Tril, I think you have to avoid using forkProcess with the threaded runtime or you get that kindf of failure
21:12:19 <geekosaur> the non-threaded runtime is fine
21:12:51 <geekosaur> lemme see if I can find the trac ticket about it
21:13:16 <Tril> geekosaur: my daemon does nothing right now, so I will try testing that without threads... but I think I will want them soon
21:15:37 <geekosaur> Tril, https://ghc.haskell.org/trac/ghc/ticket/9470 looks to be either it or related. note that there's a workaround that may help you, or may not
21:17:04 <geekosaur> Tril, there is also https://ghc.haskell.org/trac/ghc/ticket/9347 which is a different issue (basically, forkProcess is unsafe if there are any MVars or TVars in play)
21:17:09 <geekosaur> ...or maybe that is your problem
21:30:53 <Tril> geekosaur: there are no locks, the minimal test case only runs forever $ return () or forever $ threadDelay 1000000. BTW, the non-threaded runtime appears to work
21:31:52 <geekosaur> the issue in 9347 involved Handle locks in the runtime, not application code
21:32:11 <geekosaur> (and you have three of those by default: stdin stdout stderr)
21:32:53 <geekosaur> but this sounds like the 9470 issue maybe. in which case you can use the threaded runtime, start with -N1, and then setNumCapabilities (IIRC) after you're done forking
21:34:47 <Tril> geekosaur: thanks. I will also consider closing the 3 handles earlier - they get closed anyway in the grandchild

    I used "setNumCapabilities 1" instead of building with -N1, to avoid editing max.cabal, and this worked.

-}
