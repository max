=== Max 0.4 ===

http://max.tunes.org/

The Max project aims to give total control of the computer to the user.
The current status is an early UI demo, with some backend computational
parts that aren't really visible yet.  

It consists of a client and a server. See doc/INSTALL for the building
and running instructions for the server followed by the client.  Right now
the server runs only on localhost, but you can run multiple copies of the client.

Once you launch the client, you enter a 3-D world and can fly around
using WASDRF, arrow keys and mouse. You see two cubes and a sphere. 
The cubes are linked up to the inputs of an OR gate, with the sphere
linked to the output. TAB cycles between the cubes, and SPACE toggles 
the selected cube.  Red and green indicate Boolean False and True respectively.

Press ENTER to bring up a text entry box.  Click in the box and start typing;
press ENTER again to accept the text and place it in the world in front of you.
There is no way to delete or edit text except to reset the database.

Press ESC to quit.  If you restart the server and client, the world is still there.
It's stored in a database in the folder called 'world'. The database
uses the Acid-State library.  Client does not detect a down server and reconnect.

The OR gate is internally implemented as an abstract virtual machine 
made up of three NAND gates wired together with separate threads handling 
each input in parallel.  The server facilitates communication
with the simulation of the virtual machine.  The virtual machine 
supports arbitrary nesting of machines and runs in the background of
the application with inter-thread communication using 
Software Transactional Memory (STM).  Currently there is no UI to edit 
or view machines other than that provided above.

Virtual machines are first constructed with the Interpreter module, 
then simulated using the Nexus module:
    
    Introducing the Interpreter for data flow machine graph specifications.
    The Interpreter takes a graph, converts it to active components
    and runs the resulting machine in the Haskell runtime.  Interpreter
    calls upon a new module called Nexus to simplify its logic.
    
    The Nexus module provides a context (Monad) to build up a structure
    with implicit data flow, out of the basic primitive: a pipe (queue)
    for arbitrary data types.  The pipe is a pair of a Source and a Sink,
    such that that when a value is written to the sink, the source produces the data.
    Nexus then provides connections between pipes.  Nexus handles fan-out distribution
    of data when multiple sinks connect to the same source.

These components are not packaged for easy use outside of Max.

Known bugs due to limitations of the HGamer3D framework:

    - Text entry box can not be focussed automatically, you have to click to focus it.
    Reported at https://github.com/urs-of-the-backwoods/HGamer3D/issues/23
    
    - Key events are still sent to the other threads while the text edit box
    is open. This results in things like typing a space in the text box
    toggling the selected Switch, and typing WASD keys to move the camera.
    Reported at https://github.com/urs-of-the-backwoods/HGamer3D/issues/22
    
    - EditText crashes when typing 63 characters
    Reported at https://github.com/urs-of-the-backwoods/HGamer3D/issues/20
    
    - We are missing a way to detect the initial screen resolution.
    On tiling window managers, you will need to resize the window manually
    before using the text entry box, or it will appear in the wrong position.
    Reported as a pull request with code I have no idea if it will work
    at https://github.com/urs-of-the-backwoods/HGamer3D/pull/21

For detailed list of changes since 0.3, run "git log 0.3..0.4"

Bugs and Feedback:

E-mail the public mailing list max at tunes dot org.
Subscribe to the mailing list for news and discussion at:

   http://lists.tunes.org/mailman/listinfo/max

Follow @tril0byte on Twitter.  Open to direct messages.
