{-# LANGUAGE OverloadedStrings #-} -- for string literal HGamer3D Entities
{-----
 This file has been released by its author to the Public Domain.
 Details in doc/cc0.txt relative to the top level of this distribution,
 or at https://creativecommons.org/publicdomain/zero/1.0/
 -----}
import Max
import HGamer3D
import System.ZMQ4
import Control.Concurrent (yield, forkIO)
import Control.Monad (forever)
import Data.Serialize
import qualified Data.Text as T
import Data.Bits ((.&.))
import Data.List (genericIndex, genericLength)
import Data.ByteString.Char8 (pack, unpack, ByteString)

start hg3d varScreenWidth varScreenHeight = do

    ieh <- newET hg3d [
                 "eK" <: [ ctKeyEvent #: NoKeyEvent ],
                 "eM" <: [
                   ctMouse #: MMAbsolute,
                   ctMouseEvent #: NoMouseEvent],
                 "eS" <: [ ctScreenModeEvent #: ScreenModeEvent 0 0 False False ]]

    installScreenHandler hg3d (ieh # "eS") varScreenWidth varScreenHeight

    sleepFor (msecT 1000)

    -- create minimum elements, like a camera
    eCam <- newE hg3d [
        ctCamera #: FullViewCamera,
        ctPosition #: Vec3 1 1 (-30.0),
        ctLight #: Light PointLight 1.0 1000.0 1.0,
        ctOrientation #: unitU
        ]

    eText <- newE hg3d [
        ctStaticText #: "Welcome to Max! Move with W S A D R F. Space activates selection. Tab cycles selection. Mouse: Left-drag turns camera. Home key resets camera.",
        ctScreenRect #: ScreenRect 10 10 100 25
        ]

    let buttonY = 35
        buttonX = 0
        buttonHeight = 50
        buttonWidth = 150

    stickyButton <- newE hg3d [
        ctButton #: Button True "sticky",
        ctScreenRect #: ScreenRect 0 35 150 50
        ]

    actionButton1 <- newE hg3d [
        ctButton #: Button False "toggle",
        ctScreenRect #: ScreenRect 150 35 150 50
        ]

    return (eCam, ieh)

-- helper func to add object counter
populateWorld :: HG3D -> [SceneObject] -> IO [(SceneObject, [(String,Entity)])]
populateWorld hg3d objs = do
  objlist <- populateWorld' hg3d objs 0
  return $ objlist

-- recurse through world object list and add to HGamer3D scene using newE
populateWorld' :: HG3D -> [SceneObject] -> Integer -> IO [(SceneObject, [(String,Entity)])]
populateWorld' hg3d [] counter = return []
populateWorld' hg3d (obj:objs) counter = do
  entityTree <- worldObjectToEntity hg3d obj counter
  rest <- populateWorld' hg3d objs (counter + 1)
  return $ (obj,entityTree) : rest

-- n will be passed to GL when HGamer3D supports picking
worldObjectToEntity :: HG3D -> SceneObject -> Integer -> IO [(String,Entity)]
worldObjectToEntity hg3d obj n = do
  e <- case obj of
    SceneObject {obj=(Lamp lampState),pos=pos} -> do
     putStrLn ("Adding Lamp " ++ show lampState ++ " at index " ++ show n)
     newET hg3d [
      "Lamp" <| ([
        ctGeometry #: ShapeGeometry Sphere,
        ctMaterial #: (case lampState of
          True -> matGreen
          False -> matRed
          ),
        ctScale #: Vec3 10.0 10.0 10.0,
        ctPosition #: Vec3 (x pos) (y pos) (z pos),
        ctOrientation #: unitU
        ],
        [ () -: [
          ctText3D #: Text3D "Fonts/BlueHighway.ttf" 12 FCRotateXYZ False,
          ctLabel #: "Lamp",
          ctScale #: Vec3 1.0 1.0 1.0,
          ctPosition #: Vec3 (0.5) (0.0) (0.5) ]]
        )]
    SceneObject {obj=(Switch switchState),pos=pos} -> do
     putStrLn ("Adding Switch " ++ show switchState ++ " at index " ++ show n)
     newET hg3d [
      "Switch" <| ([
        ctGeometry #: ShapeGeometry Cube,
        ctMaterial #: (case switchState of
          True -> matGreen
          False -> matRed
          ),
        ctScale #: Vec3 10.0 10.0 10.0,
        ctPosition #: Vec3 (x pos) (y pos) (z pos),
        ctOrientation #: unitU
        ],
        [ () -: [
          ctText3D #: Text3D "Fonts/BlueHighway.ttf" 12 FCRotateXYZ False,
          ctLabel #: "Switch",
          ctScale #: Vec3 1.0 1.0 1.0,
          ctPosition #: Vec3 (0.5) (0.0) (0.5) ]]
        )]
    SceneObject {obj=(Text str),pos=pos} -> do
      putStrLn ("Adding Text \"" ++ str ++ "\" at index " ++ show n ++ " at (" ++ show pos ++ ")")
      newET hg3d [
       "Text" <: [
        ctText3D #: Text3D "Fonts/BlueHighway.ttf" 12 FCRotateXYZ False,
        ctLabel #: T.pack str,
        ctScale #: Vec3 10.0 10.0 10.0,
        ctPosition #: Vec3 (x pos) (y pos) (z pos)
        ]]
  return e

-- require at least 1 switch in the world or it loops forever
findNextSwitch sel scene = do
  let sceneLen = genericLength scene
      newSel = if (sel + 1 == sceneLen) then 0 else sel + 1
      obj = scene `genericIndex` newSel
   in
     if toggleable (fst obj)
     then newSel
     else findNextSwitch newSel scene
{-      case obj of
        (Switch _) -> newSel
        _          -> findNextSwitch newSel scene -}

moveSelection selEntity varSelection varScene = do
                                      sel <- readVar varSelection
                                      scene <- readVar varScene
                                      let newSel   = findNextSwitch sel scene
                                      writeVar varSelection newSel
                                      let selectedObject = scene `genericIndex` newSel
                                          selPos = pos $ fst $ selectedObject
                                      setC selEntity ctPosition (Vec3 (x selPos) (y selPos - 10.0) (z selPos))
                                      return ()


-- installKeyHandler copied from HGamer3D Cuboid game
installKeyHandler hg3d varKeysUp varKeysPressed varSelection varScreenWidth varScreenHeight varEntityCmdLine selEntity zmqDealerSock ieh cam varScene = do
    let handleKeys ke = do
                            case ke of  
                                KeyUpEvent (KeyData _ _ k) -> do
                                    updateVar varKeysPressed (\keys -> (filter (\k' -> k' /= k) keys, ()))
                                    updateVar varKeysUp (\keys -> (keys ++ [k], ()))
                                    if k == "Tab" then do
                                      moveSelection selEntity varSelection varScene
                                      return ()
                                    else if k == "Space" then do
                                      sel <- readVar varSelection
                                      -- Send Command to Server
                                      let blob = encode (sel :: Integer)
                                      send zmqDealerSock [SendMore] (encode $ pack "tryToggle")
                                      send zmqDealerSock [] blob
                                      buffer <- receive zmqDealerSock
                                      let decodedbuffer = decode buffer
                                      case decodedbuffer of
                                        Left msg -> do
                                          putStrLn ("Error: " ++ msg)
                                          return ()
                                        Right status -> do
                                          putStrLn $ "Server response: " ++ show (status::Bool) -- this line is required for the program to typecheck
                                          return ()
                                    else if k == "Return" then do
                                      maybeEntityCmdLine <- readVar varEntityCmdLine
                                      case maybeEntityCmdLine of
                                       Nothing -> do
                                        -- Open command line edit box
                                        w <- readVar varScreenWidth
                                        h <- readVar varScreenHeight
                                        putStrLn $ "Enter pressed. Screen (" ++ show w ++ "," ++ show h ++ ")"

                                        eCmdLine <- newE hg3d [
                                          ctEditText #: "",
                                          ctScreenRect #: ScreenRect 3 (h - 31) (w - 6) 25 ]
                                        writeVar varEntityCmdLine (Just eCmdLine)
                                        return ()
                                       Just alreadyCmdLine -> do
                                        cmdText <- readC alreadyCmdLine ctEditText
                                        putStrLn $ "Got command: " ++ show cmdText
                                        writeVar varEntityCmdLine Nothing
                                        delE alreadyCmdLine
                                        let unpackedText = T.unpack cmdText
                                        if (length unpackedText > 0) then do
                                          camPos <- readC cam ctPosition
                                          camOrientation <- readC cam ctOrientation
                                          {- Place new text directly in front of where the camera is facing -}
                                          let (Vec3 x y z) = camPos &+ (camOrientation *. (Vec3 0.0 0.0 24.0))
                                              textPos = Position x y z
                                              textObj = SceneObject {obj=Text unpackedText, pos=textPos}
                                          tryAddObject textObj zmqDealerSock
                                          return ()
                                        else return () -- don't add empty text object. TODO: server should prevent this too.
                                    else
                                      return ()
                                KeyDownEvent (KeyData _ _ k) -> do
                                    if k == "Escape" then do
                                      putStrLn $ "Exiting"
                                      exitHG3D hg3d
                                    else return ()
                                    updateVar varKeysPressed (\keys -> (if not (k `elem` keys) then k:keys else keys, ())) >> return ()
                                _ -> return () 
    registerCallback hg3d (ieh # "eK") ctKeyEvent (\k -> handleKeys k)
    return ()

tryAddObject obj zmqDealerSock = do
  -- Send Command to Server
  send zmqDealerSock [SendMore] (encode $ pack "tryAddObject")
  send zmqDealerSock [] (encode obj)
  putStrLn ("tryAddObject: Waiting for server...")
  buffer <- receive zmqDealerSock
  putStrLn ("tryAddObject: Received server response")
  let decodedbuffer = decode buffer
  case decodedbuffer of
    Left msg -> do
      putStrLn ("Error: " ++ msg)
      return ()
    Right status -> do
      putStrLn $ "Server response: " ++ show (status::Bool) -- this line is required for the program to typecheck
      return ()

installMouseHandler hg3d eCam ieh = do
    let handleMouse mo = do
         case mo of
          MouseButtonUpEvent (MouseButtonData button buttons qualifiers) -> do
            return ()
          MouseButtonDownEvent (MouseButtonData button buttons qualifiers) -> do
            return ()
          MouseMoveEvent (MouseMoveData mx my mdx mdy buttons qualifiers) -> do
            if (buttons .&. 1) == 1
            then do
              rotateCamera eCam (0.01 * fromIntegral mdx) (0.005 * fromIntegral mdy)
            else
              return ()

            return ()
          MouseWheelEvent (MouseWheelData wheel buttons qualifiers) -> do
            return ()
          _ -> do
            return ()
    registerCallback hg3d (ieh # "eM") ctMouseEvent (\m -> handleMouse m)
    return ()

{- Rotate camera function used by the mouse handler -}
rotateCamera eCam dx dy = do
  rotateCameraX eCam dy {- Mouse moved vertically -> rotate camera vertically along its X axis -}
  rotateCameraY eCam dx {- Mouse moved horizontally -> rotate camera horizontally along its Y axis -}

rotateCameraX eCam dx = do
  curOrientation <- readC eCam ctOrientation -- readVar camOri
  let theRotation = rotU vec3X dx
      newOrientation =  curOrientation .*. theRotation
  setC eCam ctOrientation newOrientation
  putStrLn ("updateCamera: new orientation: " ++ show newOrientation)
  return ()

{- curOrientation .*. theRotation above, vs
   theRotation .*. curOrientation below:
  Somehow swapping the quaternions between the two axes, prevents a drift when moving in a circle -}

rotateCameraY eCam dy = do
  curOrientation <- readC eCam ctOrientation --readVar camOri
  let theRotation = rotU vec3Y dy
      newOrientation = theRotation .*. curOrientation
  setC eCam ctOrientation newOrientation
  putStrLn ("updateCamera: new orientation: " ++ show newOrientation)
  return ()

-- installMoveCamera copied from HGamer3D Cuboid game
-- camera movement
installMoveCamera cam varKeysPressed = do
    let mUX = (Vec3 0.3 0 0.0)
    let mUZ = (Vec3 0 0 0.3)
    let mUY = (Vec3 0 0.3 0)
    let move = do
                    keys <- readVar varKeysPressed
                    camOrientation <- readC cam ctOrientation
                    if "D" `elem` keys then updateC cam ctPosition (\v -> v &+ (camOrientation *. mUX)) else return ()
                    if "A" `elem` keys then updateC cam ctPosition (\v -> v &- (camOrientation *. mUX)) else return ()
                    if "W" `elem` keys then updateC cam ctPosition (\v -> v &+ (camOrientation *. mUZ)) else return ()
                    if "S" `elem` keys then updateC cam ctPosition (\v -> v &- (camOrientation *. mUZ)) else return ()
                    if "R" `elem` keys then updateC cam ctPosition (\v -> v &+ (camOrientation *. mUY)) else return ()
                    if "F" `elem` keys then updateC cam ctPosition (\v -> v &- (camOrientation *. mUY)) else return ()
                    if "Left"  `elem` keys then rotateCameraY cam (-0.05) else return ()
                    if "Right" `elem` keys then rotateCameraY cam 0.05 else return ()
                    if "Up" `elem` keys then rotateCameraX cam (-0.05) else return ()
                    if "Down" `elem` keys then rotateCameraX cam  0.05 else return ()
                    if "Home" `elem` keys then do
                      setC cam ctOrientation unitU
                    else return ()
                    return ()
    forkIO $ forever $ move >> sleepFor (msecT 50)
    return()

installScreenHandler hg3d screen varScreenWidth varScreenHeight = do
  let screenEventHandler = \(ScreenModeEvent w h _ _) -> do
        writeVar varScreenWidth w
        writeVar varScreenHeight h
        return ()
    in
      registerCallback hg3d screen ctScreenModeEvent screenEventHandler

gameLogic zmqDealerSock zmqSubSock hg3d = do
  varKeysUp <- makeVar []
  varKeysPressed <- makeVar []
  varSelection <- makeVar 0 {- Index into list [SceneObjects] of the current selection -}
  varScreenWidth <- makeVar 800
  varScreenHeight <- makeVar 600
  varEntityCmdLine <- makeVar Nothing
  varScene <- makeVar []

  (eCam, inputHandler) <- start hg3d varScreenWidth varScreenHeight

  camOri <- readC eCam ctOrientation
  putStrLn ("Camera Orientation: " ++ show camOri)

  {- TODO: Make this invisible to prevent a brief appearance before it moves from the origin -}
  selEntity <- newE hg3d [
      ctGeometry #: ShapeGeometry Plane,
      ctMaterial #: matWhite,
      ctScale #: Vec3 10.0 10.0 10.0,
      ctPosition #: Vec3 0.0 0.0 0.0, {- place the selection square at the origin until we know where objects go -}
      ctOrientation #: unitU
      ]

  installMouseHandler hg3d eCam inputHandler
  installMoveCamera eCam varKeysPressed
  installKeyHandler hg3d varKeysUp varKeysPressed varSelection varScreenWidth varScreenHeight varEntityCmdLine selEntity zmqDealerSock inputHandler eCam varScene

  buffer <- receive zmqDealerSock
  let decodedWorld = decode buffer
  case decodedWorld of
    Left s -> do
      putStrLn ("Error: " ++ s)
      return ()
    Right sceneObjects -> do
      scene <- populateWorld hg3d sceneObjects

      putStrLn $ "Num World Objects: " ++ (show $ length scene)

      writeVar varScene scene

      {- initialize selected switch to the first in the scene now that we know where it is -}
      moveSelection selEntity varSelection varScene

      ---MAIN GAME LOOP---
      forever $ do
        putStrLn $ "Waiting for world updates"
        updateFromPub <- receive zmqSubSock    
        putStrLn $ "Received Update from Pub"
        let decodedUpdate = decode updateFromPub
        case decodedUpdate of
          Left s -> do
            putStrLn ("Error:" ++ s)
            return ()
          Right updateString ->
            dispatchUpdate zmqDealerSock zmqSubSock varScene updateString hg3d

dispatchUpdate :: Socket Dealer -> Socket Sub -> Var [(SceneObject, [(String, Entity)])] -> ByteString -> HG3D -> IO ()
dispatchUpdate zmqDealerSock zmqSubSock varScene updateString hg3d =
  case (unpack updateString) of
    "toggled" -> do
      objectIdToToggle <- receive zmqSubSock
      let decodedId = decode objectIdToToggle
      case decodedId of
        Left s -> do
          putStrLn ("Error:" ++ s)
          return ()
        Right n -> do
          putStrLn $ "The update: " ++ show (n :: Integer)
          scene <- readVar varScene
          toggleState scene n
          return ()
    "addedObject" -> do
      bufSceneObj <- receive zmqSubSock
      let decodedObj = decode bufSceneObj
      case decodedObj of
        Left s -> do
          putStrLn ("Error:" ++ s)
          return ()
        Right obj -> do
          scene <- readVar varScene
          let worldIndex = genericLength scene
          e <- worldObjectToEntity hg3d obj worldIndex
          writeVar varScene (scene ++ [(obj, e)])
          putStrLn ("Updated scene")
          return ()

toggleMaterial mat = 
  if mat==matGreen
  then matRed
  else matGreen
{-
  case mat of
    matRed -> matGreen
    matGreen -> matRed
-}

-- change color of a world object specified by index into the list
toggleState :: [(SceneObject, [(String,Entity)])] -> Integer -> IO ()
toggleState objs n = do
  let (_, entityTree) = objs `genericIndex` n
      entity = snd $ head $ entityTree
  oldcolor <- readC entity ctMaterial
  let newcolor = toggleMaterial oldcolor
  putStrLn $ "Toggling color: Old Color: " ++ show oldcolor ++ ", new color: " ++ show newcolor
  updateC entity ctMaterial toggleMaterial

main = do 

  withContext $ \context ->
    withSocket context Sub $ \zmqSubSock -> do

      -- subscribe to world updates first so they start queueing up
      connect zmqSubSock "tcp://localhost:3334"
      subscribe zmqSubSock ""

      withSocket context Dealer $ \zmqDealerSock -> do

        connect zmqDealerSock "tcp://localhost:3333"
        send zmqDealerSock [] (encode $ pack "getWorldState")

        runGame standardGraphics3DConfig (gameLogic zmqDealerSock zmqSubSock) (msecT 20)
        return ()
