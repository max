{-----
 This file has been released by its author to the Public Domain.
 Details in doc/cc0.txt relative to the top level of this distribution,
 or at https://creativecommons.org/publicdomain/zero/1.0/
 -----}
import Max
import Max.Nexus
import Max.Interpreter
import Control.Concurrent.STM
import Control.Concurrent.STM.TBMQueue
import Control.Concurrent
import Control.Monad (forever)

{- Example Machine: OR gate made from NAND gates.

in Hardware Description Language from nand2tetris:

CHIP Or {
    IN a, b;
    OUT out;

    PARTS:

    Nand(a=a,b=a,out=nota);
    Nand(a=b,b=b,out=notb);
    Nand(a=nota,b=notb,out=out);
}

-}

orGate = MachineGraph (
  [InterfaceNode ([MaxBool, MaxBool], [MaxBool]),
   PrimitiveNode NandGate,
   PrimitiveNode NandGate,
   PrimitiveNode NandGate],
  [(0,0,2,0),(0,0,2,1),
   (0,1,3,0),(0,1,3,1),
   (2,0,1,0),(3,0,1,1),
   (1,0,0,0)])

notGate = MachineGraph (
  [InterfaceNode ([MaxBool], [MaxBool]),
   PrimitiveNode NandGate],
  [(0,0,1,0), (0,0,1,1),
   (1,0,0,0)])

orGate2 = MachineGraph (
  [InterfaceNode ([MaxBool, MaxBool], [MaxBool]),
   MachineNode notGate,
   MachineNode notGate,
   PrimitiveNode NandGate],
  [(0,0,1,0),(0,1,2,0),
   (1,0,3,0),(2,0,3,1),
   (3,0,0,0)])

passThru = MachineGraph (
  [InterfaceNode ([MaxBool], [MaxBool])],
  [(0,0,0,0)])

debug = putStrLn

main :: IO ()
main = do
  {- testqueue -}
  --testPassThru
  --testNaive
  --testNotGate
  testOrGate

reader :: TBMQueue Bool -> IO ()
reader q = do
  threadDelay 1000000
  val <- atomically $ readTBMQueue q
  putStrLn $ show val
  return ()


testNaive = do
  let qLen = 16
  q1 <- newTBMQueueIO qLen
  threadId <- forkIO $ forever $ reader q1
  putStrLn ("Forked thread, writing queue")
  atomically $ writeTBMQueue q1 False
  threadDelay 6000000
  writer q1
  threadDelay 6000000

writer :: TBMQueue Bool -> IO ()
writer q = do
  threadDelay 3000000
  atomically $ writeTBMQueue q True

testPassThru =
  simulate (construct passThru) >>= \(([input], [output]), nexdata) ->
--    putStrLn (show threads) >>
    atomically (input True) >>
    atomically output >>= \r ->
    putStrLn (show r)

testNotGate = do
  let impl = construct notGate
  (([input], [output]), nexdata) <- simulate impl
  atomically $ input False
  r <- atomically $ output
  putStrLn (show r)
  return ()

testOrGate = do
  let impl = construct orGate
  (([inputA, inputB], [output]), nexdata) <- simulate impl
  debug("simulation launched")

  atomically $ do
    inputA False
    inputB False
  debug("sent inputs")
  finalResult <- atomically output
  putStrLn $ "Received result: " ++ show finalResult
  return ()

testqueue = do
  let qLen = 4
  q <- newTBMQueueIO qLen
  atomically $ writeTBMQueue q True
  atomically $ writeTBMQueue q True
  atomically $ writeTBMQueue q False
  atomically $ writeTBMQueue q True
  mr <- atomically $ readTBMQueue q
  case mr of
    Nothing -> putStrLn "Zilch"
    Just r -> putStrLn (show r)
  mr <- atomically $ readTBMQueue q
  case mr of
    Nothing -> putStrLn "Zilch"
    Just r -> putStrLn (show r)
  mr <- atomically $ readTBMQueue q
  case mr of
    Nothing -> putStrLn "Zilch"
    Just r -> putStrLn (show r)
  mr <- atomically $ readTBMQueue q
  case mr of
    Nothing -> putStrLn "Zilch"
    Just r -> putStrLn (show r)
