{-# LANGUAGE DeriveGeneric, DeriveDataTypeable, GeneralizedNewtypeDeriving,
  TemplateHaskell, TypeFamilies, OverloadedStrings #-}
{-----
 This file has been released by its author to the Public Domain.
 Details in doc/cc0.txt relative to the top level of this distribution,
 or at https://creativecommons.org/publicdomain/zero/1.0/
 -----}

module Max (
  WorldObject(..)
  , World(..)
  , SceneObject(..)
  , Position(..)
  , origin
  , toggleable
  -- export atomic operations with their TemplateHaskell versions
  , toggleSceneObject
  , ToggleSceneObject(..)
  , addObject
  , AddObject(..)
  , worldList
  , WorldList(..)
  , setLamp
  , SetLamp(..)
) where

import GHC.Generics
import Data.Serialize
import Data.Acid
--import Data.IxSet
import Data.Typeable
import Data.Data
import Data.SafeCopy
--import Data.Vect.Float
import Control.Monad.State as S
import Control.Monad.Reader (ask)
import Data.List (genericSplitAt)

data WorldObject = Switch Bool | Lamp Bool | Text String
  deriving (Show, Generic, Data, Ord, Eq, Typeable)
{- Serialization for network protocol -}
instance Serialize WorldObject
{- Serialization for persistent storage -}
$(deriveSafeCopy 0 'base ''WorldObject)

data Position = Position { x :: Float, y :: Float, z :: Float }
  deriving (Show, Generic, Data, Ord, Eq, Typeable)
instance Serialize Position
$(deriveSafeCopy 0 'base ''Position)

data SceneObject = SceneObject {
  obj :: WorldObject
  , pos :: Position
--, rot :: Quaternion
} deriving (Show, Generic, Data, Ord, Eq, Typeable)
instance Serialize SceneObject
$(deriveSafeCopy 0 'base ''SceneObject)

newtype World = World { worldContents :: [SceneObject] }
$(deriveSafeCopy 0 'base ''World)

toggle :: SceneObject -> SceneObject
toggle SceneObject { obj=(Switch x), pos=pos } = SceneObject {obj=Switch (not x), pos=pos }
-- due to type, have to allow toggling all objects. do nothing if toggle not supported.
toggle SceneObject { obj=s, pos=pos } = SceneObject { obj=s, pos=pos }

toggleable :: SceneObject -> Bool
toggleable (SceneObject { obj=s }) = toggleableobj s

toggleableobj :: WorldObject -> Bool
toggleableobj (Switch _) = True
toggleableobj _ = False

addObject :: SceneObject -> Update World ()
-- Add an object to the scene DB atomically.
-- Since scene is implemented as a list, we append it to the end (inefficient, but this will
-- change to an IxSet before we need efficiency).
-- Not sure how to return the index it's at from the Update monad so we use (genericLength scene) for now.
addObject obj = do
  World sceneObjs <- S.get
  let newSceneObjs = sceneObjs ++ [obj]
      newWorld = World newSceneObjs
    in
      S.put newWorld
  return ()

{- Toggles specified object by scene index ID.
 -}
toggleSceneObject :: Integer -> Update World ()
toggleSceneObject switchId = do
  World sceneObjs <- S.get
  let pieces = switchId `genericSplitAt` sceneObjs {- switch should be head of snd list -}
      {-thingToToggle = head (snd pieces)
      toggledThing = toggle thingToToggle
      wasThingAffected = thingToToggle == toggledThing
-}
      sceneObjects = fst pieces ++ toggle (head (snd pieces)) : tail (snd pieces)
      newWorld = World sceneObjects
    in
      S.put newWorld
  return ()

worldList :: Query World [SceneObject]
worldList = worldContents <$> ask

setLamp :: Bool -> Update World ()
--WARNING XXX FIXME:Lamp must be the first object in the scene
setLamp lampState = do
  World (_: switches) <- S.get
  let newWorld = World $ SceneObject { obj=Lamp lampState, pos=origin } : switches
    in S.put newWorld

origin = Position { x=0.0, y=0.0, z=0.0 }

-- if you add an operator here, add the capitalized version to exports above
$(makeAcidic ''World ['toggleSceneObject, 'worldList, 'setLamp, 'addObject])
