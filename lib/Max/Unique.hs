{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE CPP #-}

{-
 Copyright   :  (c) David E. Manifold 2018
 Copyright   :  (c) The University of Glasgow 2001
 Licensed under GHC License, see below

 This file was derived from a copy of Unique from base-4.5.0.0
 (the latest STM version before Data.Unique switched to atomicModifyIORef',
 as we anticipate STM will handle well the planned
 recursive Address structure built from nested Uniques),
 and modified to generalize Unique by making Sources a parameter.
 This file, as a derived work, is subject to the GHC license,
 embedded below since this is the only file we are copying from base.
---
The Glasgow Haskell Compiler License

Copyright 2004, The University Court of the University of Glasgow.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

- Neither name of the University nor the names of its contributors may be
used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY COURT OF THE UNIVERSITY OF
GLASGOW AND THE CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
UNIVERSITY COURT OF THE UNIVERSITY OF GLASGOW OR THE CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

 -}

#ifdef __GLASGOW_HASKELL__
{-# LANGUAGE MagicHash, DeriveDataTypeable #-}
#endif

-----------------------------------------------------------------------------
-- |
-- Module      :  Data.Unique
-- Copyright   :  (c) The University of Glasgow 2001
-- License     :  BSD-style (see the file libraries/base/LICENSE)
-- 
-- Maintainer  :  libraries@haskell.org
-- Stability   :  experimental
-- Portability :  non-portable
--
-- An abstract interface to a unique symbol generator.
--
-----------------------------------------------------------------------------

module Max.Unique (
   -- * Unique objects
   Unique,              -- instance (Eq, Ord)
   UniqueSource,
   newUniqueSource,
   newUnique,           -- :: IO Unique
   --hashUnique           -- :: Unique -> Int
   getLastUnique,
   setLastUnique
 ) where

import Prelude

--import System.IO.Unsafe (unsafePerformIO)

#ifdef __GLASGOW_HASKELL__
import GHC.Base
import GHC.Num
import GHC.Conc
import Data.Typeable
#endif

-- | An abstract unique object.  Objects of type 'Unique' may be
-- compared for equality and ordering and hashed into 'Int'.
newtype Unique = Unique Integer deriving (Eq,Ord,Show
#ifdef __GLASGOW_HASKELL__
   ,Typeable
#endif
   )

type UniqueSource = TVar Integer 

newUniqueSource :: IO UniqueSource
newUniqueSource = newTVarIO 0

-- | Creates a new object of type 'Unique'.  The value returned will
-- not compare equal to any other value of type 'Unique' returned by
-- previous calls to 'newUnique', when given the same UniqueSource.  
-- There is no limit on the number of times 'newUnique' may be called.
newUnique :: UniqueSource -> IO Unique
newUnique uniqSource = atomically $ do
  val <- readTVar uniqSource
  let next = val+1
  writeTVar uniqSource $! next
  return (Unique next)

-- SDM (18/3/2010): changed from MVar to STM.  This fixes
--  1. there was no async exception protection
--  2. there was a space leak (now new value is strict)
--  3. using atomicModifyIORef would be slightly quicker, but can
--     suffer from adverse scheduling issues (see #3838)
--  4. also, the STM version is faster.

-- | Hashes a 'Unique' into an 'Int'.  Two 'Unique's may hash to the
-- same value, although in practice this is unlikely.  The 'Int'
-- returned makes a good hash key.
{-hashUnique :: UniqueSource -> Unique -> Int
#if defined(__GLASGOW_HASKELL__)
hashUnique uniqSource (Unique i) = do
  j <- readTVarIO uniqSource
  return $ I# (hashInteger $ i + j)
#else
hashUnique (Unique u) = fromInteger (u `mod` (toInteger (maxBound :: Int) + 1))
#endif
-}

-- | Returns a copy of the last unique created.  Only for use to get the state
-- of the counter to restore later with setLastUnique.  Do not use this to 
-- try to access the previous unique.
getLastUnique :: UniqueSource -> IO Integer
getLastUnique uniqSource = readTVarIO uniqSource

-- | Restores the state of the counter used to generate new uniques.
-- Only use with a value previously returned from getLastUnique.
-- Do not use this to set the counter to a lower value, or you will get duplicates
-- of your earlier unique values.
-- This is needed in order to store the UniqueSource persistently.
setLastUnique :: UniqueSource -> Integer -> IO ()
setLastUnique uniqSource n = atomically $ writeTVar uniqSource n
