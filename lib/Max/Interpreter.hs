{-# LANGUAGE FlexibleContexts, ExistentialQuantification #-}

module Max.Interpreter (
  MachineSpec
  , Node (..)
  , Edge (..)
  , InputType
  , OutputType
  , FromNode
  , ToNode
  , FromInput
  , ToOutput
  , MaxBool (..)
  , MachineGraph (..)
  , PrimitiveMachine (..)
  , construct
  , simulate
  ) where

import Debug.Trace
import Max.Unique
import qualified Max.Nexus as N
import Control.Monad
import Control.Monad.IO.Class -- defined in transformers package until base-4.9.0.0
import Data.List (genericIndex)
import Control.Concurrent (forkIO, ThreadId)
import Control.Concurrent.STM

debug = liftIO . putStrLn

type InputID = Unique
type OutputID = Unique

-- First node (index 0) in list must be an InterfaceNode holding the connections
-- to the world outside this machine.

data Node = InterfaceNode ([InputType], [OutputType]) |
            PrimitiveNode PrimitiveMachine |
            MachineNode MachineGraph
type InputType   = MaxBool
type OutputType  = MaxBool
type FromNode    = Integer
type FromInput   = Integer
type ToNode      = Integer
type ToOutput    = Integer
type Edge        = (FromNode, FromInput, ToNode, ToOutput)
newtype MachineGraph = MachineGraph ([Node], [Edge])

{- | evalNode: begin evaluating one node in a MachineSpec in the background.
   Return control immediately.  Return a handle to manage the background task.
-}
evalNode :: Node -> N.Nexus Bool IO ([N.MaxSink], [N.MaxSource])
evalNode node = do
  case node of
    PrimitiveNode prim ->
      construct prim
    InterfaceNode _ ->
      error "evaluating interface node should not happen"
    MachineNode spec ->
      -- interpret does not launch any background tasks directly
      -- but just calls evalNode and connect which may fork.
      construct spec

nodes :: MachineGraph -> [Node]
nodes (MachineGraph g) = fst g

edges :: MachineGraph -> [Edge]
edges (MachineGraph g) = snd g

data MaxBool = MaxBool

nand a b = not (a && b)

qLen = 16 -- ^ default queue length between machine nodes

class MachineSpec s where
  construct :: s -> N.Nexus Bool IO ([N.MaxSink], [N.MaxSource])

data PrimitiveMachine = NandGate

instance MachineSpec PrimitiveMachine where
-- construct :: PrimitiveMachine -> N.Nexus Bool IO ([N.MaxSink], [N.MaxSource])
 construct prim =
  case prim of
    NandGate -> do
      (inputAsource, inputAsink) <- N.pipe qLen
      (inputBsource, inputBsink) <- N.pipe qLen
      (outputSource, outputSink) <- N.pipe qLen
      ([output], [inputA, inputB]) <- N.makeInterface ([outputSink], [inputAsource, inputBsource])
      let loop = forever $ atomically $ do
            maybeA <- inputA
            case maybeA of
              Nothing -> error "Closed Pipe"
              Just a -> do
                maybeB <- inputB
                case maybeB of
                  Nothing -> error "Closed Pipe"
                  Just b ->
                    output (a `nand` b)
        in do
          debug("created a NAND gate from " ++ show inputAsource ++ " and " ++ show inputBsource ++ " to " ++ show outputSink)
          liftIO (forkIO loop) >>= N.trackThread
          return ([inputAsink, inputBsink], [outputSource])

interface (InterfaceNode iotup) = iotup

{- | construct an instance of a machine
     in the Nexus monad,
     from a graph specification consisting of
     a list of nodes, and a list of edges;
     by calling construct on each node;
     calling Nexus.pipe for each vertex;
     calling Nexus.connect for each edge;
     connecting everything together;
 -}

instance MachineSpec MachineGraph where
  construct spec =
    let nodeSpecs         = nodes spec
        edgeSpecs         = edges spec
        interfaceSpec     = head nodeSpecs
        internalNodeSpecs = tail nodeSpecs
        ioSpec            = interface interfaceSpec
        inputTypes        = fst ioSpec
        outputTypes       = snd ioSpec
      in do
        {- create a pipe for each input and output with the outside world -}
        inputPipes        <- traverse (\_ -> {- ignore this machine's input types as Bool is the only one -}
                                 N.pipe qLen) inputTypes
        debug("inputPipes: " ++ show inputPipes)
        outputPipes       <- mapM (\_ -> {- ignore this machine's output types as Bool is the only one -}
                                 N.pipe qLen) outputTypes
        debug("outputPipes: " ++ show outputPipes)
        {- N.pipe returns (MaxSource, MaxSink) -}
        let inputSources     = map fst inputPipes
            inputSinks       = map snd inputPipes
            outputSources    = map fst outputPipes
            outputSinks      = map snd outputPipes
            outsideInterface = (inputSinks, outputSources)
            insideInterface  = (outputSinks, inputSources)
        {- construct inside nodes, which return their interfaces as ([Sink], [Source]) -}
        internalFrames       <- mapM evalNode internalNodeSpecs
        let frames           =  insideInterface : internalFrames
        debug ("frames: " ++ show frames)
        debug ("inputSources: " ++ show inputSources)
        debug ("inputSinks: " ++ show inputSinks)
        debug ("outputSources: " ++ show outputSources)
        debug ("outputSinks: " ++ show outputSinks)
        {- recurse over edgeSpecs; connect source outputs to sink inputs -}
        mapM_ (\(sourceNodeIndex,sourceIndex,sinkNodeIndex,sinkIndex) -> do
          debug ("sourceNodeIndex: "++show sourceNodeIndex)
          debug ("sourceIndex: "++show sourceIndex)
          debug ("sinkNodeIndex: "++show sinkNodeIndex)
          debug ("sinkIndex: "++show sinkIndex)
          let sourceFrame = (frames `genericIndex` sourceNodeIndex)
              d1 = trace ("sourceframes: "++show(length sourceFrame))
              sinkFrame   = (frames `genericIndex` sinkNodeIndex)
              sinks       = fst sinkFrame
              sources     = snd sourceFrame
              sink        = sinks `genericIndex` sinkIndex
              source      = sources `genericIndex` sourceIndex
            in
              N.connect source sink)
          edgeSpecs

        return outsideInterface

{- simulate a machine implementation built by 'construct'
   by an action in the IO monad,
   providing an STM interface to the machine
 -}
simulate :: N.Nexus Bool IO ([N.MaxSink], [N.MaxSource]) -> IO ([Bool -> STM ()], [STM (Maybe Bool)])
simulate machine = N.evalNexus $ machine >>= N.makeInterface
