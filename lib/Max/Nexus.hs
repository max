{-# LANGUAGE GeneralizedNewtypeDeriving, MultiParamTypeClasses, FunctionalDependencies, FlexibleContexts #-}
{- | Nexus: a distribution hub for MaxPipes

Nexus provides a context in which data flow becomes implicit;
it happens automatically and without thinking or worrying
about how it's done.  It resembles UNIX pipe(2) except with
any Max data type, instead of only streams of bytes.
Pipes in Nexus may be created and connected at runtime and
operate between threads.  Haskell Conduit provides a clever
way of structuring a Haskell program, but to run a Conduit
it must be fully specified, which is a non-starter for
dynamically building data flow structures at runtime, and
swapping parts in and out while they are running.

Nexus specializes in creating large numbers of the very common agent
("a connection") that pulls from a source and pushes to a sink. The
interface leaves it open to Nexus to decide whether to implement its
set of connections using polling (if supported by the underlying Pipe)
or a thread per source, or some mixture thereof.

Usage:
Create a Nexus, and create some sources and sinks with MaxPipe.
Then, in the Nexus, "connect" a source with a sink.  Nexus will create
any needed threads to ensure that the data from each connected source
reaches all sinks connected to it (one-to-many fan-out).

To export a source or sink from Nexus, running the Nexus and making it usable outside,
use the output or input functions.

 -}

module Max.Nexus (
  Nexus
  , MaxSource
  , MaxSink
  , pipe
{- ^ a FIFO queue: a pair of Sink and Source such that publishing an element
   to the Sink in one thread makes it available on the Source which may run
   in a different thread.
-}
  , connect
{- ^ Attach a source to a sink such that elements published by the source
     flow to the sink.  A sink may be connected to 0 or 1 sources.
-}
  , makeInterface
  , evalNexus
  , trackThread
 ) where

import Control.Concurrent (forkIO, ThreadId, myThreadId)
import Control.Concurrent.STM
import Control.Concurrent.STM.TBMQueue
import Control.Monad.State.Strict
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map) -- To avoid writing the stupid-looking Map.Map
import qualified Data.Set as Set
import Data.Set (Set) -- To avoid writing the stupid-looking Set.Set
import Max.Unique
import Control.Applicative -- needed for Alternative instance

-- | PipeId: A distinct FIFO queue in the nexus, providing Sink and Source
type PipeId = Unique
--  deriving (Eq, Ord)
-- | MaxSource: An opaque object that may only read from its pipe
--TODO: link to its Nexus. Should this be an Address?
newtype MaxSource = MaxSource { sourcePipe :: PipeId }
  deriving (Eq, Ord, Show)
-- | MaxSink: An opaque object that may only write to its pipe
newtype MaxSink = MaxSink { sinkPipe :: PipeId }
  deriving (Eq, Ord, Show)

{- | The Nexus monad: a computation which may include a nexus,
     which runs in the monad 'm', and returns a result of type 'a'.

     A nexus manages data flow between sources and sinks of type t
     that have previously been "connected" together in the nexus.
     Data flows asynchronously in background threads.
 -}
newtype Nexus t m a = Nexus { unNexus :: StateT (NexData t) m a }
  deriving (MonadTrans
          , Monad
          , Functor
          , Applicative
          , MonadState (NexData t)
          , MonadFix
          , MonadIO
          , Alternative
          , MonadPlus
          )

evalNexus :: (MonadIO m) => Nexus t m a -> m a
evalNexus n = newNexData >>= evalStateT (unNexus n)

data NexData t = NexData {
  -- | Counter for unique pipe identifiers
    pipeIdGenerator :: UniqueSource

  {- | Map from each PipeId to its underlying TBMQueue.
       Necessary because we can not use a TBMQueue
       as the key for a Map without an "instance Ord TBMQueue"
       We also can't put TBMQueues into a Set but the handler needs
       to lookup the actual queues so we will pass the map of queues in also.
   -}
  , pipeQueues      :: TVar (Map PipeId (TBMQueue t))

  {- | Map from each Source to its local state, consisting of
       a tuple of the ThreadId of the thread handling that source,
       and the TVar Set of all Sinks currently connected to that Source.
       The thread handler will read this TVar each time it receives data,
       to determine the current Set of Sinks to push data to.

   -}
  , activeSources   :: Map MaxSource (ThreadId, TVar (Set MaxSink))

  , trackedThreads    :: [ThreadId]  -- ^ trackThread tracks other threads using this Nexus for GC protection
  }

showActiveSources :: (MonadIO m) =>
                     Map MaxSource (ThreadId, TVar (Set MaxSink)) ->
                     Nexus t m String
showActiveSources srcs = do
  {- first go through the map and replace (ThreadId, TVar (Set MaxSink) with String -}
  sinkDescriptions <- mapM procsource srcs
  {- go through again and display the keys and values -}
  let str = Map.mapWithKey (\k v -> "ActiveSource {source: " ++ show k ++ "," ++ v ++ "}")
              sinkDescriptions
  return $ Map.foldr (++) "" str

procsource :: (MonadIO m) =>
              (ThreadId, TVar (Set MaxSink)) ->
              Nexus t m String
procsource (tid, tsinks) = do
  sinks <- liftIO $ atomically $ readTVar tsinks
  return $ "threadId:" ++ show tid ++
           ",sinks:" ++ show sinks ++ "}"

showNexus :: (MonadIO m) => Nexus t m String
showNexus = do
 nd <- get
 showNexData nd

showPipes :: (MonadIO m) => TVar (Map PipeId (TBMQueue t)) -> Nexus t m String
showPipes tvpq = do
  pipeQueues <- liftIO (atomically (readTVar tvpq))
  return ( "Queues:" ++ show ( Map.size pipeQueues )  )

showNexData :: (MonadIO m) => NexData t -> Nexus t m String
showNexData nd = do
 srcDescs    <- showActiveSources (activeSources nd)
 pipeDescs   <- showPipes (pipeQueues nd)
 let str     =  "NexData { "
             ++ pipeDescs
             ++ srcDescs
             ++ show (trackedThreads nd)
             ++ "}"
 return str

{- | Create a NexData structure in any monad stack built on IO
     using initial empty values, suitable to
     pass into a new Nexus computation -}
newNexData :: (MonadIO m) => m (NexData t)
newNexData = do
  uniqueSource <- liftIO $ newUniqueSource
  pipeQueues   <- liftIO $ newTVarIO Map.empty
  return NexData { pipeIdGenerator = uniqueSource
                 , pipeQueues      = pipeQueues
                 , activeSources   = Map.empty
                 , trackedThreads  = [] }

pipe :: MonadIO m => Int -> Nexus t m (MaxSource, MaxSink)
pipe qlength = do
  nexdata <- get
  pipeId <- liftIO $ newUnique (pipeIdGenerator nexdata)
  queue <- liftIO $ newTBMQueueIO qlength

  liftIO $ atomically $ modifyTVar' (pipeQueues nexdata) (Map.insert pipeId queue)

  -- Since everything modified was in a TVar, we don't actually "put" a new Nexus..
  liftIO $ putStrLn ("pipe: created pipe id " ++ show pipeId)
  -- Returning source first, the same order as POSIX pipe(2)
  return (MaxSource pipeId, MaxSink pipeId)

trackThread :: (MonadIO m) => ThreadId -> Nexus t m ()
trackThread tid = do
  nexdata <- get
  put NexData { pipeIdGenerator =       (pipeIdGenerator nexdata)
              , pipeQueues      =       (pipeQueues      nexdata)
              , activeSources   =       (activeSources   nexdata)
              , trackedThreads  = tid : (trackedThreads  nexdata) }
  return ()

{- | Register a connection from source to sink in this Nexus.
     If sink is already connected to this source, do nothing.
     If sink is connected to another source, undefined
     Add the sink to the set of sinks connected to this source.
     If this is the first listener to this source, spawn a thread.
 -}
connect :: MonadIO m => MaxSource -> MaxSink -> Nexus t m ()
connect sourceid sinkid = do
  liftIO $ putStrLn ("connect " ++ show sourceid ++ " " ++ show sinkid)
  nexdata <- get
  let sourceState = Map.lookup sourceid (activeSources nexdata)
  case sourceState of

    Just (threadId, sinks) -> do
      -- Connect a new sink to a source that is already being monitored.
      liftIO $ atomically $ modifyTVar' sinks (Set.insert sinkid)
      liftIO $ putStrLn ("connect: Add sink to source already monitored")
      str <- showNexData nexdata
      liftIO $ putStrLn str
      return ()

    Nothing -> do

      -- Connect a sink to a source that is not being monitored.

      -- Set up a new set of sinks in a variable to share with the handler
      newSinks <- liftIO $ newTVarIO $ Set.singleton sinkid

      -- Get TBMQueue of supplied sink so we can pass it directly to the handler
      queues <- liftIO $ atomically $ readTVar (pipeQueues nexdata)

      let targetSourcePipe = Map.lookup (sourcePipe sourceid) queues
      case targetSourcePipe of

        Nothing -> error "Invalid Sink"

        Just sourceQueue -> do

          liftIO $ putStrLn ("connect: first time adding a sink to this source")
          -- Spawn a new Haskell thread to handle connections from this source
          threadId <- liftIO $ forkIO $
            sourceHandler sourceQueue newSinks (pipeQueues nexdata)

          -- Update the Nexus to include the new thread and sinks
          let newSources = Map.insert sourceid (threadId, newSinks)
                             (activeSources nexdata)
              newNexData = NexData { activeSources=newSources
                                   , pipeIdGenerator=(pipeIdGenerator nexdata)
                                   , pipeQueues=(pipeQueues nexdata)
                                   , trackedThreads=threadId : (trackedThreads nexdata) }
            in do
              put newNexData
              str <- showNexData newNexData
              liftIO $ putStrLn str

  return ()

{- | handler loop for each thread -}
sourceHandler :: TBMQueue t                     -- ^ the source to read
              -> TVar (Set MaxSink)             -- ^ which sinks to write
              -> TVar (Map PipeId (TBMQueue t)) -- ^ to look up queue of a sinkid
              -> IO ()
sourceHandler sourcePipe destSinks pipeQueues =
    loop
  where
    loop = do
      mti <- myThreadId
      putStrLn("sourceHandler " ++ show mti ++ " waiting")
      val <- atomically $ readTBMQueue sourcePipe
      putStrLn("sourceHandler got value")
      case val of
        Nothing -> error "Queue is closed"
        Just thing -> do
          connectedSinks <- atomically $ readTVar destSinks
          putStrLn("sourceHandler notifying queues: " ++ show connectedSinks)
          queues <- atomically $ readTVar pipeQueues
          mapM_ (writeOneSink thing queues) connectedSinks
          loop

    writeOneSink somedata queues (MaxSink sinkPipeId) = do
      let sinkQueue = Map.lookup sinkPipeId queues
      case sinkQueue of
        Nothing -> error "Invalid Sink"
        Just queue -> atomically $ writeTBMQueue queue somedata
      return ()

{- | Make an interface for the outside world to communicate to the Nexus.
     makeInterface takes a list of sinks and sources,
     and produces corresponding lists of STM actions to access them outside the Nexus.
     For each sink, you get a function taking 1 argument of type t and returning an STM action that
     writes the item to the sink, retrying if the sink's queue is full.
     If the queue is closed the item is silently discarded.
     For each source, you get an STM action that reads the source and returns an item of type (Maybe t),
     retrying if the queue is empty. (Just t) is returned when the item is available.
     Nothing is returned if the queue is closed already -}
makeInterface :: (MonadIO m) => ([MaxSink], [MaxSource]) -> Nexus t m ([t -> STM ()], [STM (Maybe t)])
makeInterface (inputSinks, outputSources) = do
  liftIO $ putStrLn ("makeInterface: inputSinks: " ++ show inputSinks ++ ", outputSources: " ++ show outputSources)
  nexdata <- get
  queues <- liftIO $ atomically $ readTVar (pipeQueues nexdata)
  return (inputs queues inputSinks, outputs queues outputSources)

inputs :: (Map PipeId (TBMQueue t)) -> [MaxSink] -> [t -> STM ()]
inputs queues sinks = map (input queues) sinks

input :: (Map PipeId (TBMQueue t)) -> MaxSink -> t -> STM ()
input queues snk = do
  let pipeId = (sinkPipe snk)
      sinkQueue = Map.lookup pipeId queues
  case sinkQueue of
    Nothing -> error "Invalid sink"
    {- partially evaluate writeTBMQueue: produces a function taking 1 argument: the item to write -}
    Just q -> writeTBMQueue q

outputs :: (Map PipeId (TBMQueue t)) -> [MaxSource] -> [STM (Maybe t)]
outputs queues sources = map (output queues) sources

output :: (Map PipeId (TBMQueue t)) -> MaxSource -> STM (Maybe t)
output queues src = do
  let pipeId = (sourcePipe src)
      sourceQueue = Map.lookup pipeId queues
  case sourceQueue of
    Nothing -> error "Invalid source"
    Just q -> readTBMQueue q

